<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\Stock;
use Validator;
class StockController extends Controller
{
    //
   public function store(Request $request){
    	$validator=Validator::make($request->all(), [ 
            'symbol' => 'required|string'
                     
            
        ]);
        
        if($validator->fails()){
        	 return response()->json(['error'=>$validator->errors()], 401);
        }
        
        $user=$request->user();
        $stock=Stock::where('symb',$request->symbol)->first();
       	
        if($stock===null){
        	$stock=new Stock();
        	$stock->symb=$request->symbol;
        	$stock->save();
        	
        }
        if(!$user->stocks()->where('stock_id', $stock->id)->exists()){
        	$user->stocks()->attach($stock);
        }
        
    }
    public function get(Request $request){
        
        $recomendations=[];
        $user=$request->user();
        $stocks=$user->stocks;
        
        foreach ($stocks as $stock) {
            $candidate_stocks=[];
            $users=$stock->users()->where('user_id','!=',$user->id)->get();
            foreach ($users as $user_r) {
               $user_stocks=$user_r->stocks()->where('stock_id','!=',$stock->id)->get();
               if(count($candidate_stocks)==0){
                    for($i=0;$i<count($user_stocks);$i++){
                        array_push($candidate_stocks, $user_stocks[$i]);
                    }
               }
             
                foreach ($user_stocks as $item) {
                       $i=0;
                       $notFound=true;

                       while($notFound && $i<count($candidate_stocks)){

                            if($candidate_stocks[$i]->symb==$item->symb){
                                $notFound=false;

                            }

                            else{
                                $i=$i+1;


                            }

                       }

                       if($notFound){
                        array_push($candidate_stocks,$item);


                    }


                }

               
            }
            $recomanded= (object)['position'=>[],'rank'=>0];
            for($n=0;$n<count($candidate_stocks);$n++){
                $rank=0;
                foreach ($users as $user_r) {

                 $user_stocks=$user_r->stocks()->where('stock_id','!=',$stock->id)->get();
                 foreach ($user_stocks as $stock) {
                   if($stock->symb ==$candidate_stocks[$n]->symb ){
                    $rank=$rank+1;
                   }
                 }
                }
                if($rank>=$recomanded->rank){
                    array_push($recomanded->position, $n);
                    $recomanded->rank=$rank;
                    
                }

            }
            if($recomanded->rank !=0){
                foreach ($recomanded->position as $position) {

                   array_push($recomendations, $candidate_stocks[$position]);
                }

            }
        }

        for($z=0;$z<count($stocks);$z++){
            for($l=0;$l<count($recomendations);$l++){
                if($recomendations[$l]->symb==$stocks[$z]->symb){
                    array_splice($recomendations,$l,1);
                }
            }
        }
        return response()->json(['recommendation'=>$recomendations], 200);
        
        
        
    }

}
