<?php

namespace App\Http\Controllers;
use Validator;
use Illuminate\Support\Facades\Auth; 
use App\User;
use Illuminate\Http\Request;

class UserController extends Controller
{
    //
    public $successStatus = 200;
    public function login(Request $request){

    	$validator=Validator::make($request->all(), [ 
            'email' => 'required|email', 
            'password' => 'required|min:8', 
        ]);
        if ($validator->fails()) { 
            return response()->json(['error'=>$validator->errors()], 401);            
        }
        if(Auth::attempt(['email' => request('email'), 'password' => request('password')])){
        	$user=Auth::user(); 
        	$success['token'] =  $user->createToken('welcome to our awesome tradding app')-> accessToken; 
            

        	return response()->json(['success' => $success], $this-> successStatus); 
        }
        else{
        	return response()->json(['error'=>'Unauthorised'], 401);
        } 
     }
     public function signUp(Request $request){
     	
     	$validator=Validator::make($request->all(), [ 
            'email' => 'required|email|unique:users', 
            'password' => 'required|min:8', 
            'nom'=>'required|string',
            'prenom'=>'required|string',
            'birth_date'=>'required|date'


        ]);
        if ($validator->fails()) { 
            return response()->json(['error'=>$validator->errors()], 400);            
        }
        $user=new User(
        [
            'nom' => $request->nom,
            'prenom'=>$request->prenom,
            'email' => $request->email,
            'password' => bcrypt($request->password),
            'birth_date'=>$request->birth_date
        ]);

        $user->save();
        
        $success['token'] =  $user->createToken('welcome to our awesome tradding app')-> accessToken; 
        return response()->json(['success' => $success], $this-> successStatus);
        
     }

     public function infos(Request $request){
        $user = Auth::user(); 
        return response()->json(['nom' => $user->nom,"prenom"=>$user->prenom,"email"=>$user->email], $this-> successStatus);
        
      
     }
}
