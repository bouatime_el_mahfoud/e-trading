import React from 'react';
import ReactDOM from 'react-dom';
import * as serviceWorker from './serviceWorker'
import { BrowserRouter,Route,Switch} from 'react-router-dom'
import 'bootstrap/dist/css/bootstrap.min.css'
import $ from 'jquery';
import Popper from 'popper.js';
import {App} from './components/App'
import {Dashboard } from './components/Dashboard'
import {ListStocks } from './components/ListStocks'
import {StockDashboard } from './components/StockDashboard'
import 'bootstrap/dist/js/bootstrap.bundle.min'
import './stylesheets/index.css'



ReactDOM.render(
	<div className="container-fluid bg-white" id="the-full-page" >
		<div className="row" >
			<div className="col-12">
				<BrowserRouter>
					<Switch>
						<Route path="/" component={App} exact/>
						<Route path="/dashboard"  component={Dashboard} exact/>
						<Route path='/dashboard/:name' component={ListStocks}/>
						<Route path='/stocks/:symbol' component={StockDashboard} />
					</Switch>
				</BrowserRouter>
			</div>
		</div>
	</div>
	, document.getElementById('root'));

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: http://bit.ly/CRA-PWA
serviceWorker.unregister();
