import React from 'react';
import {Line} from 'react-chartjs-2';
import {FaDoorOpen} from "react-icons/fa";
import {FaDoorClosed} from "react-icons/fa";
import { IoMdTrendingUp } from "react-icons/io";
import { IoMdTrendingDown } from "react-icons/io";
import {OpeningPriceStatistics} from './OpeningPriceStatistics'
import {ClosingPriceStatistics} from './ClosingPriceStatistics'
import {MaxPriceStatistics} from './MaxPriceStatistics'
import {MinPriceStatistics} from './MinPriceStatistics'
import "../stylesheets/statisticsDashboard.css"


export class StatisticsDashboard extends React.Component{
	constructor(props){
		super(props)
		this.state={
			type:"1"
		}
		this.onSwitch=this.onSwitch.bind(this)
	}
	onSwitch(link:number){
		this.setState({type:link})
		
	}


	render(){


	return (
		<div className="row bg-light container-stats" >

			<div className="col-11  text-center "  >

				
				<div className='row container-stats align-items-center'>
					
						{(this.state.type==1)?<OpeningPriceStatistics symbol={this.props.symbol}/>:(this.state.type==2)?<ClosingPriceStatistics symbol={this.props.symbol}/>:(this.state.type==3)?<MaxPriceStatistics symbol={this.props.symbol}/>:(this.state.type==4)?<MinPriceStatistics symbol={this.props.symbol}/>:<span></span>}
					
						
					
				</div>

				

			</div>
			<div className='col-1 bg-info'>
					<div className="row links">
						<div className='col-10 offset-1 text-center text-white btn' onClick={()=>this.onSwitch(1)}>
								<FaDoorOpen/>
						</div>
					</div>
					<div className="row links">
						<div className='col-10 offset-1 text-center text-white btn' onClick={()=>this.onSwitch(2)}>
								<FaDoorClosed/>
						</div>
					</div>
					<div className="row links">
						<div className='col-10 offset-1 text-center text-white btn' onClick={()=>this.onSwitch(3)}>
								<IoMdTrendingUp/>
						</div>
					</div>
					<div className="row links">
						<div className='col-10 offset-1 text-center text-white btn' onClick={()=>this.onSwitch(4)}>
								<IoMdTrendingDown/>
						</div>
					</div>

			</div>
		</div>

		)
		}
	}
