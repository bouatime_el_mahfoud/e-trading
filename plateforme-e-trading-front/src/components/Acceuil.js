import React from 'react'
import { Welcome } from './Welcome'
import { LoginMenu } from './LoginMenu'
export class Acceuil extends React.Component{
	
	render(){
		return(
			<div className='row'>
				<div className="col-sm-6">
					<Welcome />
				</div>
				<div className='col-sm-6'>
					<LoginMenu isLogin={this.props.isLogin} onLogin={this.props.onLogin} onAuth={this.props.onAuth} />
				</div>
				
			</div>
		)
	}
	
}