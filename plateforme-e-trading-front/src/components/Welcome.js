import React from 'react'
import '../stylesheets/welcome.css'
import { FaFacebookF } from "react-icons/fa"
import {FaLinkedin} from "react-icons/fa"
import { IoMdMail } from "react-icons/io"
export const Welcome=()=>{
	return(<div className="row align-items-center" id="welcome-full">
			<div className="col-12 ">
				<div className='row' id="description">
						<div className="col-12 ">
						<h1 className="text-center text-white " id="heading">E-Tradding S5 Project</h1>
						<p className="text-center text-white lead" > 
							Bienvenue sur la plateforme du E-Tradding  développée par <span className="font-weight-light">Bouatim </span> et <span className="font-weight-light">Zerif </span>
						</p>
						</div>
				</div >
				<div className="row" >
					<div className="col-4 text-center">
						<button className="btn btn-outline-primary btn-lg btn-block "><FaFacebookF/> <span className="text-to-hide">Follow Us</span></button>
					</div>
					<div className="col-4 text-center">
						<button className="btn btn-outline-info btn-lg btn-block "><FaLinkedin/> <span  className="text-to-hide">Follow Us</span> </button>
						
					</div>
					<div className="col-4 text-center">
						<button className="btn btn-outline-danger btn-lg btn-block t"><IoMdMail/> <span className="text-to-hide">Contact Us</span> </button>
					</div>
				</div>
			</div>
		   </div>
		)
}