import React , {Component} from 'react'
import { FaUserCircle }  from "react-icons/fa";
import { FaCalendarAlt } from "react-icons/fa";
import { FaLock } from "react-icons/fa";
import { MdMailOutline } from "react-icons/md";
import { IoIosLaptop } from "react-icons/io";

import '../stylesheets/login.css'
export class Login extends Component{
	constructor(props){
		super(props)
		this.save=this.save.bind(this)
		this.onLogin=this.onLogin.bind(this)
	}
	onLogin(token){
		this.props.onAuth(token)
	}
	save(e){
		e.preventDefault()
		if(this._email.value.length>=0 && this._password.value.length>=8){
			fetch('http://localhost:8000/api/login', {
  			method: 'POST',
  			headers: {
    			'Accept': 'application/json',
    			'Content-Type': 'application/json',
  			},	
  			body:JSON.stringify({
          	"email": this._email.value,
          	"password": this._password.value,
          	
        })
  			})
  			.then(response => response.json())
  			.then(data=>{
  				if(data.success){
  					alert("connexion valid")
  					this.onLogin(data.success.token)
  				}
  				else alert("connexion echoué")
  			})
		}
		else{
			alert("la taille de mot de passe doit etre superieur ou egal à 8 caracteres")
		}
	}

	render(){


	return(
			<div className="row align-items-center " id="full-login">
				
				<form className="col-10 offset-1" onSubmit={this.save}>
		  			<div className="form-group row" >
		  				<div className="col-12 text-center">

		  					<p className="lead "><IoIosLaptop/> Se Connecter</p>

		  				</div>
		  			</div>
		  			<div className="form-group row field-log-in">
		    			<label for="email" className="col-4 col-form-label  text-secondary"><MdMailOutline/> Email</label>
		    			<div className="col-8">
		      				<input type="email" className="form-control" id="email" ref={input=>this._email=input} placeholder="email" required/>
		    			</div>
		  			</div>
		  			<div className="form-group row field-log-in">
		    			<label for="password" className="col-4 col-form-label  text-secondary"><FaLock/> Mot de passe</label>
		    			<div className="col-8">
		      				<input type="password" className="form-control" id="password" ref={input=>this._password=input}  placeholder="min: 8 caractéres" required/>
		    			</div>
		  			</div>
		  			<button type="submit" className="btn btn-info" >Connexion</button>


		</form>
				
			</div>
		)
	}
}