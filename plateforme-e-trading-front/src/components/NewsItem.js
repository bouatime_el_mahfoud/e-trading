import React from 'react'
export const NewsItem=(props)=>{
	return(

			<div className="row">
				<hr/>
				<h5 className="col-6 text-center text-truncate lead">{props.headline}</h5>
				<h4 className="col-4  text-truncate">{props.datetime}</h4>
				<a className="btn btn-primary col-2 text-truncate" href={props.url} target='_blank'>Consulter</a>
				<hr/>
			
			</div>
	
		)
}