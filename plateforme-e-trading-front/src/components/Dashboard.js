import React from 'react'
import { FaClock }   from "react-icons/fa"
import {IoIosCheckmarkCircleOutline} from "react-icons/io"
import {IoIosLogOut} from "react-icons/io"
import { Sector } from './Sector'
import "../stylesheets/dashboard.css"
import { Redirect } from 'react-router'
import { ListRecomendations } from './ListRecomendations'
import {Link } from "react-router-dom"
import Modal from 'react-awesome-modal'
export class Dashboard extends React.Component{
	constructor(props){
		super(props)
		this.state={
			sectors:[],
			fullName:"",
			email:"",
			recomendations:[],
			visible:false
		}
		this.eachSectore=this.eachSectore.bind(this)
	}

	componentWillMount() {
		if(this.props.location.state && this.props.location.state.isAuth){
			fetch('https://api.iextrading.com/1.0/stock/market/sector-performance')
     	    .then(response => response.json())
      		.then(data => this.setState({sectors:data}))
      		fetch('http://localhost:8000/api/infos', {
  			method: 'GET',
  			headers: {
    			'Accept': 'application/json',
    			'Content-Type': 'application/json',
    			'Authorization':'Bearer '+this.props.location.state.token
  			}
  			
  			}
  			)
  			.then(response => response.json())
      		.then(data => {
      			this.setState({
      			fullName:data.nom+" "+data.prenom,
      			email:data.email
      		})
      		})
      		fetch('http://localhost:8000/api/recomendation', {
  			method: 'GET',
  			headers: {
    			'Accept': 'application/json',
    			'Content-Type': 'application/json',
    			'Authorization':'Bearer '+this.props.location.state.token
  			}
  			
  			}
  			)
  			.then(response => response.json())
      		.then(data => {
      			this.setState({
      				recomendations:data.recommendation
      		});
      			if(this.state.recomendations.length!=0){
      				this.setState({
      				visible:true
      		});
      			}
      		})
      		
		}
		

	}
	logout(){
		this.props.location.state.isAuth=false

		this.props.history.push({
 		pathname: '/',
  		state: { logout: true }
		})
	}
	
	
	eachSectore(sector,key){
		return(<div className='col-md-3'>
				<Sector name={sector.name} augmentation={sector.performance} lastupdated={sector.lastUpdated} token={this.props.location.state.token} isAuth={this.props.location.state.isAuth}/>
				</div>)
	}
	openModal() {
        this.setState({
            visible : true
        });
    }

    closeModal() {
        this.setState({
            visible : false
        });
    }
	
	render(){
		if(this.props.location.state && this.props.location.state.isAuth){
			
			return(
			<div className="row " id="dashboard-full">
				<div className="col-12">
					
					<Modal className="row" visible={this.state.visible} effect="fadeInUp"  onClickAway={() => this.closeModal()}>
                   				<ListRecomendations  recomendations={this.state.recomendations} token={this.props.location.state.token} isAuth={this.props.location.state.isAuth}/>
                	</Modal>
					
					<div className="row bg-info" id='the-nav-Dashboard'>
						<div className="col-6 text-truncate">
							<h6 className='text-white lead text-capitalize'>{this.state.fullName}</h6>
							<h6 className="text-light lead ">{this.state.email}</h6>
						</div>
						<div className="col-6 ">
								<div className="row text-right">
									<input type="text" className="formControlName col-10 col-sm-10" placeholder="cherchez un marché " />
									
									
									<span className="col-2 text-danger text-left btn " onClick={()=>this.logout()}><IoIosLogOut /></span>
								</div>
						</div>
					</div>
					<div className='row'  id='markets'>
					
						{this.state.sectors.map(this.eachSectore)}
					</div>

				</div>
			</div>
			)
		}
		else{
			return(<Redirect to='/'/>)
		}
		
	}
}