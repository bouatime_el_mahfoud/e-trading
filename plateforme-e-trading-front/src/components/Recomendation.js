import React from 'react'
import {Link } from "react-router-dom"
import { FaRegThumbsUp } from "react-icons/fa"
export const Recomendation=(props)=>{
	var route="/stocks/"+props.symbol
	return(

		<Link  to={{ pathname: route, state: { isAuth: props.isAuth,token:props.token }}}  className="row text-dark " id="container-item" >
			<h4 className="col-12 text-left lead text-white"><span className="text-info"><FaRegThumbsUp/></span> {props.symbol}</h4>
		</Link>
		)
}