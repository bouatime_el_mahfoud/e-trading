import React , {Component} from 'react'
import { Login } from './Login'
import { SignUp } from './SignUp'
import '../stylesheets/loginMenu.css'
export class LoginMenu extends Component{
	constructor(props){
		super(props)
		this.onSwitch=this.onSwitch.bind(this)
	}
	onSwitch(){
		this.props.onLogin()
	}

	render(){
		return(
				<div className="row bg-light" id="login-menu-app">
						<div className='col-12'>
							<div className="row">
									<div className="col-12">
											{(this.props.isLogin) ? <Login onAuth={this.props.onAuth}/> :<SignUp onLogin={this.onSwitch}/>}
									</div>
							</div>
							<div className="row ">
								<div className="col-6 offset-6 text-right" >
										{(this.props.isLogin)?<button className="btn btn-link text-dark" onClick={this.onSwitch}>S'inscrire ?</button> : <button class="btn btn-link text-dark" onClick={this.onSwitch}>se connecter ?</button>}
								</div>
							</div>
						</div>
						
				</div>
				
				
			)
	}
}