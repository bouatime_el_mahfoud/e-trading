import React from 'react'
import '../stylesheets/stockitem.css'
import {Link } from "react-router-dom"
export const StockItem=(props)=>{
	var route="/stocks/"+props.symbol;
	const stockViewed=()=>{
		fetch('http://localhost:8000/api/store', {
  			method: 'POST',
  			headers: {
    			'Accept': 'application/json',
    			'Content-Type': 'application/json',
    			'Authorization':'Bearer '+props.token

  			},	
  			body:JSON.stringify({
          	"symbol": props.symbol,
          	
          	
        })
  			})
  			
	}
	return(
		<Link to={route} className="row text-dark btn " id="container-item" onClick={()=>stockViewed()}>
		<li className="col-12 list-group-item btn " id="stockItem-full">
			<span className="row ">
				<label className="col-2 text-center lead text-truncate btn"> {props.symbol} </label>
				<label className="col-2 text-center lead text-truncate btn">{props.companyName}</label>
				<label className="col-2 text-center lead text-truncate btn"> 
{props.primaryExchange} </label>
				<label className="col-2 text-center lead text-truncate btn">{props.calculationPrice}</label>
				<label className="col-2 text-center lead text-truncate btn">{props.high}</label>
				<label className="col-2 text-center lead text-truncate btn">{props.low}</label>
			</span>
			
		</li>
		</Link>
		
		)
}