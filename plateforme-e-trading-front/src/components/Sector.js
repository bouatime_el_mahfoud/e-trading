import React from 'react'
import { FaEye }  from "react-icons/fa"
import {FaArrowUp } from "react-icons/fa"
import { FaArrowDown} from "react-icons/fa"
import { FiActivity} from "react-icons/fi"
import {Link } from "react-router-dom"
export const Sector=({augmentation,lastupdated,name,token,isAuth})=>{
	 var time = new Date().getTime();
     var date = new Date(time);
     var route="/dashboard/"+name;
	return(
		
		<div className="card" >
  						<div className="card-body border border-success" >
    						<h5 className="card-title"><span className='text-secondary'><FiActivity/></span> Marché</h5>
    						<h6 className="card-subtitle mb-2 text-muted text-success">{name}</h6>
    						<blockquote className="blockquote">
  								<p className="mb-0 lead">Ce marché connait un taux d'augmentation:{(augmentation>0) ? <span className="text-success"><FaArrowUp/> {augmentation}% </span>:<span className="text-danger"><FaArrowDown/>{augmentation}%</span>}</p>
  								<footer className="blockquote-footer text-right">Mise à jour :<cite title="Source Title">{date.toLocaleString()}</cite></footer>
							</blockquote>
							<Link to={{
            pathname: route,
            state: {token:token,isAuth:isAuth }
        }}><button className="btn btn-primary"><FaEye /> Consulter</button></Link>
  						</div>
		</div>
		)
}