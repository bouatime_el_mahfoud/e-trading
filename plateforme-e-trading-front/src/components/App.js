import React from "react"
import { Dashboard } from './Dashboard'
import { Acceuil } from './Acceuil'
import { Redirect } from 'react-router'

import "../stylesheets/app.css"
export class App extends React.Component{
	constructor(props){
		super(props)
		this.state={
			isAuth:false,
			token:"",
			isLogin:true
		}
		this.updateIsLogin=this.updateIsLogin.bind(this)
		this.updateIsAuth=this.updateIsAuth.bind(this)
	}
	componentWillMount() {
		if(this.props.location.state){
			this.setState({
					isAuth:false,
					token:"",
					isLogin:true
			})
			
		}
	}
	updateIsLogin(){
		this.setState(
		{
			isLogin:!this.state.isLogin
		}
		)
	}
	updateIsAuth(token){
		this.setState(
		{
			isAuth:!this.state.isAuth,
			token:token

		}
		)
	}
	render(){
		console.log(this.props)
		return(
					<div className="row bg-info align-items-center" id="app-full">
						<div className="col-12 col-sm-10 offset-sm-1 rounded" id="app-div">
							{(!this.state.isAuth)? <Acceuil isLogin={this.state.isLogin} onLogin={this.updateIsLogin} onAuth={this.updateIsAuth}/>:<Redirect to={{
            pathname: '/dashboard',
            state: { isAuth: this.state.isAuth,token:this.state.token }
        }} />}
						</div>
					</div>
			)
	}
}
