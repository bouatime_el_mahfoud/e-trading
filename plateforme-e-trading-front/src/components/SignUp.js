import React , {Component} from 'react'
import { FaUserCircle }  from "react-icons/fa";
import { FaCalendarAlt } from "react-icons/fa";
import { FaLock } from "react-icons/fa";
import { MdMailOutline } from "react-icons/md";
import { FaUserPlus } from "react-icons/fa";

import '../stylesheets/signup.css'
export class SignUp extends React.Component{
	constructor(props){
		super(props)
		this.save=this.save.bind(this)
		this.onSignUp=this.onSignUp.bind(this)
	}
	onSignUp(){
		this.props.onLogin()
	}

	save(e){
		
		e.preventDefault()
		if(this._password.value.length>=8 && this._prenom.value.length>0 && this._nom.value.length>0 && this._birthDate.value.length>0 && this._email.value.length>0){
			
			fetch('http://localhost:8000/api/signup', {
  			method: 'POST',
  			headers: {
    			'Accept': 'application/json',
    			'Content-Type': 'application/json',
  			},	
  			body:JSON.stringify({
          	"email": this._email.value,
          	"password": this._password.value,
          	"nom":this._nom.value,
          	"prenom":this._prenom.value,
          	"birth_date":this._birthDate.value
        })
  			})
  			.then(response => response.json())
  			.then(data=>{
  				if(data.success){
  					alert("inscription valide")
  					this.onSignUp()

  				} 
  				else{

  					alert("les erreurs : "+data.error.email[0])
  				} 
  			})

		}
		else alert("la taille de mot de passe doit etre superieur ou egal à 8 caracteres")

	}
	render(){


		return(
				<div className="row align-items-center " id="full-sign-up">
					
					<form className="col-10 offset-1" onSubmit={this.save}>
			  			<div className="form-group row" id="sign-up-heading">
			  				<div className="col-12 text-center">

			  					<p className="lead "><FaUserPlus/> Cree un Compte</p>

			  				</div>
			  			</div>
			  			<div className="form-group row field-sign-up">
			    			<label for="prenom" className="col-4 col-form-label text-secondary"><FaUserCircle /> Prenom</label>
			    			<div className="col-8">
			      				<input type="text"  ref={input=>this._prenom=input} className="form-control" id="prenom" placeholder="Prenom" required/>
			    			</div>
			  			</div>
			  			<div className="form-group row field-sign-up">
			    			<label for="nom" className="col-4 col-form-label  text-secondary"><FaUserCircle /> Nom</label>
			    			<div className="col-8">
			      				<input type="text" ref={input=>this._nom=input} className="form-control" id="nom" placeholder="Nom" required/>
			    			</div>
			  			</div>
			  			<div className="form-group row field-sign-up">
			    			<label for="date" className="col-4 col-form-label  text-secondary"><FaCalendarAlt/> Date naissance</label>
			    			<div className="col-8">
			      				<input type="date" ref={input=>this._birthDate=input} className="form-control" id="date" placeholder="Nom" required/>
			    			</div>
			  			</div>
			  			<div className="form-group row field-sign-up">
			    			<label for="email" className="col-4 col-form-label  text-secondary"><MdMailOutline/> Email</label>
			    			<div className="col-8">
			      				<input type="email" ref={input=>this._email=input} className="form-control" id="email" placeholder="email" required/>
			    			</div>
			  			</div>
			  			<div className="form-group row field-sign-up">
			    			<label for="password" className="col-4 col-form-label  text-secondary"><FaLock/> Password</label>
			    			<div className="col-8">
			      				<input type="password" ref={input=>this._password=input} className="form-control" id="password" placeholder="min: 8 caractéres" required/>
			    			</div>
			  			</div>
			  			<button type="submit" className="btn btn-info">S'inscrire</button>


			</form>
					
				</div>
			)
	}
}