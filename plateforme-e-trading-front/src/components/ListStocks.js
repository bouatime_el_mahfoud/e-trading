import React from 'react'
import { FaClock,FaBlackTie,FaRegThumbsUp }   from "react-icons/fa"
import {IoIosCheckmarkCircleOutline} from "react-icons/io"
import { TiImageOutline } from "react-icons/ti"
import { StockItem } from './StockItem'
import {IoIosLogOut} from "react-icons/io"
import {IoIosPulse} from "react-icons/io"
import { Redirect } from 'react-router'
import {Link } from "react-router-dom"
import { MdSwapVert,MdTrendingDown,MdTrendingUp} from 'react-icons/md'
import "../stylesheets/listStocks.css"
export class ListStocks extends React.Component{
	constructor(props){
		super(props)
		this.state={
			stocks:[],
			fullName:"",
			email:""
		}
		this.eachStock=this.eachStock.bind(this)
	}
	componentWillMount() {
		if(this.props.location.state && this.props.location.state.isAuth){
		var link='https://api.iextrading.com/1.0/stock/market/collection/sector?collectionName='+this.props.match.params.name
		fetch(link)
      .then(response => response.json())
      .then(data => this.setState({stocks:data}))
      fetch('http://localhost:8000/api/infos', {
  			method: 'GET',
  			headers: {
    			'Accept': 'application/json',
    			'Content-Type': 'application/json',
    			'Authorization':'Bearer '+this.props.location.state.token
  			}
  			
  			}
  			)
  			.then(response => response.json())
      		.then(data => {
      			this.setState({
      			fullName:data.nom+" "+data.prenom,
      			email:data.email
      		})
      		})
  	}

	}
	eachStock(stock,key){
		return <StockItem  symbol={stock.symbol} companyName={stock.companyName} primaryExchange={stock.primaryExchange} calculationPrice={stock.calculationPrice} high={stock.high} low={stock.low} isAuth={this.props.location.state.isAuth} token={this.props.location.state.token} />
				
	}

	render(){
		if(this.props.location.state && this.props.location.state.isAuth){
		return(
				<div className="row " id="stocks-full">
				<div className="col-12">
					<div className="row bg-info" id='the-nav-Dashboard'>
						<div className="col-6 text-truncate">
							<h6 className='text-white lead text-capitalize'>{this.state.fullName}</h6>
							<h6 className="text-light lead">{this.state.email}</h6>
						</div>
						<div className="col-6 ">
								<div className="row ">
									<input type="text" className="formControlName col-10 col-sm-10" placeholder="cherchez une action " />
									
									<span className="col-2 text-danger text-left btn"><Link className="text-white"  to={{pathname: '/',state: { logout:true}}} ><IoIosLogOut /></Link></span>
								</div>
						</div>
					</div>
					<div className='row' >
						{(this.state.error)?<div className='col-md-12 text-center'>
							<h1>une erreur s est produit</h1>
							<p>veuillez verifiez monsieur est ce que vous avez accées à internet !</p>
							<button className='btn btn-danger'>Quitter</button>
						</div>:<div className="col-sm-12">
								<div className="row">
									<h2 className="col-md-12 text-secondary text-center text-Capitalize "><IoIosPulse/>{this.props.match.params.name} </h2>
								</div>
								<div className='row' id="stocks">
									<ul className="col-12 list-group list-group-flush">
										  <li className="row list-group-item ">
										  	<label className="col-2 text-center lead text-truncate"><TiImageOutline/> Symbol entreprise </label>
										  	<label className="col-2 text-center lead text-truncate"><FaBlackTie/> Entreprise</label>
										  	<label className="col-2 text-center lead text-truncate"><FaRegThumbsUp/> échange primaire </label>
										  	<label className="col-2 text-center lead text-truncate"><MdSwapVert/> ouvert</label>
										  	<label className="col-2 text-center lead text-truncate"><MdTrendingUp/> prix max</label>
										  	<label className="col-2 text-center lead text-truncate"><MdTrendingDown/> prix min </label>
										  </li>
										  {this.state.stocks.map(this.eachStock)}
									</ul>
								</div>
						</div>
						}
					</div>

				</div>
			</div>
			)
		}
		else{
			return(<Redirect to='/'/>)
		}

	}

}