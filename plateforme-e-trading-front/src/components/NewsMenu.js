import React from 'react'
import {NewsItem} from './NewsItem'
import {FaRegNewspaper } from "react-icons/fa"
export class NewsMenu extends React.Component{
	constructor(props){
		super(props)
		this.state={
			news:[]
		}
		this.eachNews=this.eachNews.bind(this)
	}
	componentWillMount() {
		console.log(this.props.symbol)
		fetch('https://api.iextrading.com/1.0/stock/'+this.props.symbol+'/news/last/5')
     	    .then(response => response.json())
      		.then(data => this.setState({news:data}))
	}
	eachNews(news,key){
		return(	<div className="col-10 offset-1">
				<NewsItem headline={news.headline} url={news.url} datetime={news.datetime}/>
				</div>
				)
	}
	render(){
		return (
			<div className="col-12">
				<div className="row ">
					<h1 className="col-12 text-center " ><span className='text-info'><FaRegNewspaper /></span> Menu News</h1>
				</div>
				<br />
				<div className='row'>

					{this.state.news.map(this.eachNews)}
				</div>
				<br/>
				

			</div>
			)
	}
}