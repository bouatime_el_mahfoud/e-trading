import React from 'react';
import {CompanyInfos} from './CompanyInfos'
import {StatisticsDashboard} from './StatisticsDashboard'
import "../stylesheets/stockDashboard.css"
export const StockDashboard=(props)=>{
	var symbol=props.match.params.symbol;
	return (
		<div className="row " id='stockdashboard-container'>

			<div className="col-md-4 " id="company-infos-container">
				<CompanyInfos symbol={symbol} />
			</div>
			<div className='col-md-8 border ' >
				<StatisticsDashboard symbol={symbol}/>
			</div>
		</div>
		)
}