import React from 'react'
import { FaStar } from "react-icons/fa"
import { Recomendation } from './Recomendation'
export class ListRecomendations extends React.Component{

	constructor(props){
		super(props)
		this.eachRecomendation=this.eachRecomendation.bind(this)

	}
	eachRecomendation(recomendation,key){
		return(<div className='col-12'>
				<Recomendation  symbol={recomendation.symb} isAuth={this.props.isAuth} token={this.props.token}/>
				</div>)
	}
	render(){
		return(
				<div className='col-12 bg-secondary'>
					<div className="row">
						<h2 className="col-12 text-center  text-white"> <span className="text-warning"><FaStar/></span> Menu Recommendations</h2>
					</div>
					<div>
						{this.props.recomendations.map(this.eachRecomendation)}
					</div>
				</div>
				)
	}
}