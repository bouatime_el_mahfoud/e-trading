import React from 'react'

import {Line} from 'react-chartjs-2';
export class PredictionCalcul extends React.Component{
		constructor(props){
			super(props)
			this.trainNetwork=this.trainNetwork.bind(this)
		}
		
	
		trainNetwork(dataset:any[],lengthgenerate,trainingDataLength,normalizedData,initial_step){
			const brain=require("brainjs")
			var finalModels=[]

			var trainingDataSets=[]
			for(var i=0;i<dataset.length;i++){
				var networkinputs=[]
				for(var j=0;j<dataset[i].length;j++){
					var data=[]
					for(var k=0;k<dataset[i][j].length-1;k++){
						data.push(dataset[i][j][k])
					}
					
					networkinputs.push({
						input:data,output:[dataset[i][j][dataset[i][j].length-1]]
					})
					
				}
				trainingDataSets.push(networkinputs)

			}
			
			console.log(trainingDataSets)
			var k=initial_step
			for(var i=0;i<trainingDataSets.length;i++){

				var net = new brain.NeuralNetwork();
				net.train(trainingDataSets[i])
				
				var result=[]
				var params=[]
				for(var n=k;n>0;n--){
				params.push(normalizedData[trainingDataLength-n])
				}
				
				for(var t=0;t<lengthgenerate;t++){
					var output=net.run(params)
					result.push(output[0])
					
					for(var f=0;f<params.length-1;f++){
					params[f]=params[f+1]
					}
					params[params.length-1]=output[0]
					

				}
				k=k+initial_step
				finalModels.push(this.deNormalizeData(result))
			
				
				
				


			}

			return finalModels

				
			


		}
		createDataSets(predictionsPrices){
			console.log(predictionsPrices)
		}
		addCells(){
			var labels=this.props.stocks_date.slice()
			for(var i=0;i<this.props.stocks_date.length;i++){
				var label=this.props.stocks_date[this.props.stocks_date.length-1]+" +"+(i+1)+"j"
				labels.push(label)
			}
			return labels
			
		}


		normalizeData(){
			var result=[]
			var max =Math.max.apply(null, this.props.stocks)
			var min=Math.min.apply(null,  this.props.stocks)
			for(var s=0;s<this.props.stocks.length;s++){
				var normalized=(this.props.stocks[s]-min)/(max-min)
				result.push(normalized)
			}
			return result

		}
		deNormalizeData(output){
			var result=[]
			var max =Math.max.apply(null, this.props.stocks)
			var min=Math.min.apply(null,  this.props.stocks)
			for(var s=0;s<output.length;s++){
				var denormalized=output[s]*(max-min) +min
				result.push(denormalized)
			}
			return result

		}

		recalculatePrices(trainingDataLength,prediction){
			
			var pricesModels=[]
			for(var i=0;i<prediction.length;i++){
					var prices=this.props.initial_stock.slice(0,trainingDataLength)
					var initial_price=this.props.initial_stock[trainingDataLength-1]
					for(var j=0;j<prediction[i].length;j++){
						var price=prediction[i][j]*100+initial_price;
						if(price<0) price=0
						prices.push(price)
						initial_price=price
					}
					pricesModels.push(prices)
			}
			return pricesModels
		}
		
		render(){
			if(this.props.visible){


			var normalizedData= this.normalizeData()
			var data=this.props.stocks
			const length=this.props.stocks.length
			var trainingDataLength=Math.trunc(length*0.8)
			var initial_step=Math.trunc(trainingDataLength/5)
			
			
			var resultlength=2*length-trainingDataLength
			var datasets=[]
			var k=initial_step
			while(k<trainingDataLength){
				
				var inputs=[]
				for(var j=0;j<trainingDataLength-k;j++){
					var input=[]
					for(var l=k;l>=0;l--){
						input.push(normalizedData[j+k-l])
					}
					inputs.push(input)
					
				}
				datasets.push(inputs)
				k=k+initial_step

				
				
			}
			


			var predictionsPercentChange=this.trainNetwork(datasets,resultlength,trainingDataLength,normalizedData,initial_step)
			var predictionsPrices=this.recalculatePrices(trainingDataLength,predictionsPercentChange)
			var allLabels=this.addCells();

			
			
			var data= {
        labels: allLabels,
        datasets: [{
        label: "model reel ",
        borderColor: 'rgb(0,150,136)',
        fill:false,
        data: this.props.initial_stock,
        },
        {
        label: "model 1  ",
        borderColor: 'rgb(255,235,59)',
        fill:false,
        data: predictionsPrices[0],
        },
         {
        label: "model 2  ",
        borderColor: 'rgb(33,150,243)',
        fill:false,
        data: predictionsPrices[1],
        },
         {
        label: "model 3  ",
        borderColor: 'rgb(121,85,72)',
        fill:false,
        data: predictionsPrices[2],
        },
        {
        label: "model 4  ",
        borderColor: 'rgb(244,67,54)',
        fill:false,
        data: predictionsPrices[3],
        },
        {
        label: "model 5  ",
        borderColor: 'rgb(244,67,255)',
        fill:false,
        data: predictionsPrices[4],
        }
        

        ]
    }

    	return(
			<div className="container">
						<h1>Menu Prediction</h1>
						<Line data={data} />
				
				
			</div>
			)
	}
	else return <h1>nothing</h1>
		
		
			
			
		}
}