import React from 'react';
import {FaBuilding } from "react-icons/fa"
import {FaUserTie } from "react-icons/fa"
import {IoIosGlobe } from "react-icons/io"
import {FaIndustry } from "react-icons/fa"
import {FaExchangeAlt } from "react-icons/fa"
import {FaStore } from "react-icons/fa"
import {FaRegNewspaper } from "react-icons/fa"
import { NewsMenu } from './NewsMenu'
import Modal from 'react-awesome-modal'
import "../stylesheets/companyInfos.css"
export class CompanyInfos extends React.Component{
	constructor(props){
		super(props)
		this.state={
			infos:{},
			company_icon:"",
			visible:false
		}
	}
	openModal() {
        this.setState({
            visible : true
        });
    }
    closeModal() {
        this.setState({
            visible : false
        });
    }

	componentWillMount() {
		
		var link='https://api.iextrading.com/1.0/stock/'+this.props.symbol+'/company'
		var link1='https://api.iextrading.com/1.0/stock/'+this.props.symbol+'/logo'
		
		fetch(link)
      .then(response => response.json())
      .then(data =>{this.setState({infos:data,company_icon:this.state.company_icon} );})
      fetch(link1)
      .then(response => response.json())
      .then(data =>{this.setState({infos:this.state.infos,company_icon:data.url} )})

	}
	render(){
		var website_link=""
		return(
				<div className="row">

					<div className="col-12 " >
						
							<Modal className='row' visible={this.state.visible} effect="fadeInUp" width="80%"  onClickAway={() => this.closeModal()} >
                   				<NewsMenu symbol={this.props.symbol}/>
                			</Modal>
						
						<div className="row weight-two align-items-center" >
							<div className="col-8 offset-2 text-center" >
								<img src={this.state.company_icon}  onError={(e)=>{e.target.onerror = null; e.target.src="data:image/jpeg;base64,/9j/4AAQSkZJRgABAQAAAQABAAD/2wCEAAkGBxMREhUSExMVFRUVFxYYFxcXFRcWFRgXFxcXFxYYFxcYHSggGBolGxUXITEhJSkrLi4uGB8zODMtNygtLisBCgoKDg0OGxAQGy0lICUvLy4tLS0tLS0tLy4tLS0tKy0vLS8uLS0tLy0tLS0tNi0tLS0tLS0vLS0tLS0tLS0tLf/AABEIAOEA4QMBEQACEQEDEQH/xAAbAAEAAQUBAAAAAAAAAAAAAAAAAgEDBAUGB//EAD8QAAIBAgIGBgYIBgMBAQAAAAABAgMRBCEFEhMxQVEGImFxgZEyM3KhstEHI0JSkrHB8BQVU2KC4UPC8XMW/8QAGwEBAAIDAQEAAAAAAAAAAAAAAAMFAQIEBgf/xAAyEQACAgECAwUHBAMBAQAAAAAAAQIDEQQxEiFBBRNRcYEiMmGxweHwM5Gh0RQjQgbx/9oADAMBAAIRAxEAPwD3EAAAApGVwCoAAAAAAAAAAAAAAAAAAAAABSMr5gFQAAAAAAAAAAAAAAAC3KQBKABIAAAAAAAi2AUt2gEkwCoAAAAAAAALcpAE47gCoAAAAAABFsAoASTAKgAAtykAVjEAmAAAAAAAACCAABJIAqAAAAAAAW5SuASjEAkAAAAAAAACEQAASSAKgFJIAjGIBMAAAAAAAAAAo0AEgCoAALFbEqEoqTilLKN3m5creRHK1RklJpZ28ySNblFuK238i4p77qyW533qyd+zivA2T3yaY2wTTNjBGaugCkY8QCYAAAAAAAAABRoAJAFQAAAAAAAAACM43TXNWydn4Nbg1kynh5OE0ppnGYCrs5SVWm84Occ3HlrRs9Zbne/PiVFt9+nnwt5XTJ6HT6TSayvjiuGXXD6+TzyM3A9Pab9bTlB84tTXfwfuZLDtKL99Y/kgt7EsX6ck/Pl/Z0GB03h63q6sG+V9WX4ZWZ2V6iuz3ZFbbpL6vfi18v3NgTHMAAAWq7VrOLkmnla63bmaTxjDWTaOc5TOK09pN15JajgoXST9LO29cNyyPN63VO+SWMY8d+eNz0Gj0ypi3nOf2Mvo5UqVqvXqScYxb1XJtSv1bNcVn+R0aCVl1vtybSW2d+mxBroQqr9mKy3v4dTry/KQAAAAGNg3U620SWfVtyIanZz7z0JbVXy4PUySYiAAAAAAAAAAAAAAAAAAAAANdp7RMcVSdOWT3wl92S3Pu4NciHUUq6HC/Q6dJqpaexTj6rxR5Li8PKlOVOatKLs18uw87KLi3GW6PaVWRsgpw2ZaaNcG5v8Aorp6dCtGM5ydKXVacm1G+6Svus9/Y2dml1Mq5pSfsv8AMlZ2hoY3VNwiuJc/P4fnU9RL48kACNS9na1+F91+0xLOORlYzzNJp/RjrzpqLimr6z4qLtZ245p+ZW67Su+ceHpv5Hdo9SqIycvTzJ4PQdOnPXpynrQurP0W3Hjlms08mbVaCuufFW3lftsYt1tlkOGaWH++5uUWJwlQAAAAAW69TVjKXJN+RrOXDFy8DaEeKSj4mnwOl5ymoyStJ2y4ciup1k5zUZLc7rtJGMHKPQ3hZleY2OxapJNxbu7ZEN1yqSbWSWqp2NrODJJiIAAAAAAAAAAAAAAAAHL9NdAbeG1pr62C3LfOPLvW9eK4nBrdN3keOO6/lFt2Xru5n3c37L/h/m/7nm5SHqgAem9CNL7ehqSd6lK0XzcfsS8lbvXaXuhv7yvhe6/EeS7V0vc3cUfdlz9eqOjO0rAAUsAVAABiYrSNOm7N58lm/wDRBZqa63hvmTV6eyzmkQoaWpSdrtP+5W9+40hrKpPGceZvPS2RWcZ8i/jcRs4Odr24dryRNdZ3cHIiqr7yaiajDaanrJSS1W7ZK1iur103LEtjus0UVHMXzN5K252z95aPGzK5eKMWho2nCWtFZ8M727iCGmrhLiSJp6iyceFszDoIAAAAAAAAAAAAAAAAAAAAADzvpxoDZS/iKa6kn10vsyfHub9/eU2u03A+8js9/P7npuydd3ke5m+a2+K+3y8jkyuLo2XR7Sjw1eNT7PozXOD3+KyfgT6e7urFLp18jk1umWopcOu68/vseuQkmk07pq6fBpno08ni2mnhkgYABjYvGKm4JpvWdsvD5kNtyraTW5LXU5ptPYppHEbOnKS37l3vIaix11uSFFfHYos5Ru+bKFvJeJY5IGDJvdFvbUpU5PdZX423ryaLXTPvqnXLp+Iq9Qu5tU49fxlMNoS0k5STSd7Jb+8xXoOGWZPJmzXcUcRRYrT29dJPqx49kc2/F/oRTffX8tl9NySC7mjL3f1M/B6WjUnqJNb7Pnb8jrq1cbJ8KRzW6WVcOJs2J1nKAAAAAAAAAAAAAAAAAAAAAW69GM4uEknGSaae5p70YlFSWHsbQm4SUovDR5P0i0PLC1XB5wd3CXOPJ9q3PwfE89qKHTPh6dD2Wi1cdTXxdVuvj/TNWc52HovQDSu0pOhJ9al6PNw4fheXdYuuz7uKHdvdfL7Hlu19NwWd7HaXz++/7nVlgVAAABh6Tp69OUVm1Z245Z2OfUx7ytxW5Pp5cFib2OWKIugDJlYfFOnCSi7Sk1d8opcO279xPXc64NR3fyILKlZNOWy+Zbp4ucd05Z9rf5msbpx2bNpUwlukbvQOF1Ya73y3ezw8/kWWhq4YcT6/IrtZZxT4V0+ZlYfR9OEtaMbPveXdyJ69PXCXFFEM75zjwtmUTkIAAAADYBSLvmAVAABGTAKW7wCUWAVAAAAIbWPNeaMZRnD8Cuuua8xlDDNdp7RcMVSdN2T3wl92XB93B9hDqKY3Q4X6eZ06TUy09imvVeKPL6mElRk4zWrKLafG3DLmv03FE63B+1uesVytinDZktE6SeHrxrR3J5rnB5SXlu7kKre7sU1+IajT99S63v8AXoevUqilFSi7ppNPmnmmejTTWUeKlFxeHuiZkwQrJuLtk7O3fbI1nnheNzaOMrOxzujcLUVVPVkrPrNp2txz43KjT1Wq1PDXiWmotrdTWU/AVoKtXajaMc7tclvl4sSjG69qPJf11EZOmlN83+cjB1c8s+Xdwuc2OfI6cvHMo1beg+W6C57FDVrBsnky8LpGpBrrNrk81bs5E9eqsg98rwZBZpq59MPxOlw1ZTipLc/20XVc1OKkionBwk4sum5oAAAGAW5SuATisgCoAAIoAoASSAKgAAAHFdPOj6kniacesl9Yrb4r7feuPZ3FXr9Kpf7Yrz/v86F72RrnF9xN8nt5+Hr8/M4LUXJeRUcK8D0mWXqEI8UvJNW4+JvGMSKcpPYjJ8Fu/fuDfRGyjzyyJqbHpnQDFSnhbP8A45ygn2WUl5a1vAvOz5uVOH0eDyfbFahqcrqk/p9Do2zuKs1PR3T9PGqrKmmo06jhd2tJJJqStwdzWMlI6dTpZUOKl1WfL4GTpfE7Om7b5ZL9X5EGrt7uvlu+RjTVcdnPZGl9XR/uq+6C+ZW/p0/GXy+5YfqXfCPz+xiRlZ3/AHuIM4eSfGVguSd834I3bW7NEnsiyyJvJKlgGDJ0mgYtUs+Ldu7d+aZdaJNVFPrGu9MnG1pQjeMdZ3WRNdOUI5isshqhGUsSeC9B3SbVst3IkTyjR8mSbMmC3J3AJRiASAAAAKNABIAqAAAAACjQB5h0v0D/AA1TWgvqql9X+18YfquzuKHV6bupZXuv+Pget7O1v+RXwy95fz8f7Oj6N4fDY2jepRg6kerNqKi20spXjbevfc7dNGq+vMorK5MqtbPUaS7EJvhfNc8+nPwL+I6D4WXo7SHszv8AGmbS7Ope2V6/3k0h2zqY74fmv6wa+f0fxvlXko8nBN+esl7iJ9mLPKXLy/PkdK7dljnBZ8/t9TrNGYCGHpxpU11Y897bzbb5ssKqo1xUYlPffO6bnPdnPfSNpn+HwrhF2qVrwjzUbfWS8nbvkhZLCOrs3T97dl7R5/1+fA1n0Reor/8A1XwI1p2Z0dsfqR8vqb3Fy29fV+zHLwXpP99hX2vv7+FbL8ZHUu4p4ur/ABGBpLFpuU21GEVlfJKK3d3M5rp95Pl5I6aK+CKj1+prtGaThiIuVO9k7O6tnv8A1NZwcHhk9lbi+ZscNRdSaiuOXcuIrg7JqKIbJKuDky68NtKso0llna74LjflckdSstca9iNW8FalZuZmH0HJvrtJclm38jor0Es+2+RBZro49hG9pwUUklZLJFpFKKwitbbeWSMmAARmrgCMQCQAAAAAAAAAAAABjY/FqlBzfgub4IzFZeCO2xQjk46eInJtuTu3d5s6MIqXOTeWyxio7SLhJtp9vk+80tpjbBwlsybT6qzT2q2D5r8w/M1mgsbLA4la/oSylycG7Ka7mvdJHnIOWkv4ZbdfLo/z4nvbO77S0asr33XwfVfT9meppl6eWKgAA8T6e6UeIxlT7tJulFew2pPvcr+CXI5bHmR6rs+lVULxfN+u38Gt0ZpvEYZSjRqypqTu0lF3drXzTNFJrYnt09VrzOOTLwnSrFU3fa6yas4uMOtHir6t14MjVcUmo8sms9LVPGVsT6S6cdd7ODtST/G1xfYuC8eVo6aeHm9zNNPBze5vuhlK2Gv96cn5dX/qc+peZkGoftnWYb6ulKp9qfVj3faZvX/rqc+r5L6lbZ/stUOi5v6Gy0DhtWGu98vhW75+R26GrhhxePyOTWW8U+FdPmbQ7TjNH0jx+qtlF5vOTXBcF4/veSVx6nHqrcewjn9tL70vNk2EcHE/EvYOrLaQ60vTjxf3kavY3rk+Jc+qO2OcuAAAAAAAAAAAAAAADktOY7azsn1I5LtfFk8I4RV6i3jlhbI1puc4ANljNBrFYOGrbaw13B8+s7xfY/c7FbrtN3y5brb+j0nY2temxn3Xv++/oXegulXUpOhO+0o5We/U3K/bF9V9y5kGgucocEt4/L7bHd2tplXZ3sfdl8/vudQd5UgA8x+kfoxGm5YyE4x15LWpve5ve4NcXvafa78DnthjmX/ZmrlNKlrZb/D4/I4zCYV3UpZcUuPic7kX9VTbyyGkElJWVsr35iIvST5IxjYgNphdP16ShGDSjBW1dVNPO7u9+bfBoilRGWWyKVMJNtno+jNIwx2o4dWEYpNN5wsrz1n+vHJkc4Oy1QWy+XUp51y00ZOXNv8AnwMPTfTWU6sMJo/Vc5SUNq1eC9hcUt7lusna+8sXZ/zE1o7PUYO3Ubb46+v9fudtjcUqUHOXBeb4InSy8FJZNQjxM4utUc5OTzbd2dC5FPKTk8sgDBewfrIe3H4kHsb1++vNHcnMXIAAAAAAAAAAAAANVp/HbOGon1pe6PF/p/4bwjlnNqbeGOFuzlScrCsotZPIBrBQA67QHqIf5fFIgn7xa6b9JevzNJ0kwMsNWjj6K9H10V9qO5y8t/cnwZV6mt1TV8PVfAv9Fcr6npLHv7r8H4fnxR1GHrxqRjOLvGSTT5pq6LCMlJJoqZwcJOMt0XDJqeVdMtMfxNayzp07qHJv7U/HcuxLmcNs+KXI9p2Vou4pzJe1Lm/h4L86+Rot5EWmxjYuhrJW3oyngjtr41yMengZPfl72bORDGiT3K4+MVayzfERyZvUVjCLFLETjGUYykozSU0nZSSd0nzRucrjFtNrmtjvfop0NeU8XJZRvTp979OS8LR8ZE1MepT9r6jCVS839PzyOg09jtpPVXowyXa+L/T/ANO6EcI8dqbeOWFsjWxj8/I2IEUBgvYL1kPbj8SD2N6/fXmjuTmLkAAAAAAAAAAAAt16yhFyluSuwlk1lJRWWcVjMS6s3N8eHJcEdKWEVE5ucuJmVoXBbWpn6Mc5dvJeP6M1nLCJNPVxy+CLWlvXVPaZmOxrd+ozENiI67QHqIf5fFIgn7xa6b9JevzNhKKas1dPenuZo1knTxzRptEUnhqjw3/HK86D5LfOk3zTzXNN8mctMe6l3fTdfVeny8ju1M1qIK7/AKXKX0l67P4+Zg9OtN7ClsoP6yqmvZhuk+97l48iS6fCsI6ux9F39veS92P8vovr/wDTzQ4j2IBkqDGxQAtx0fPEVadGmrznrWvksldtvgrI3rWXhHHrbI1w45bL7GTQ6F46dTZ7Bxzs5ScdRdusm7ruuSquWdisl2hp1Hi4s/DqenVoxwWGp4am81HVT4/3zfa22+9nbVA8jr9U5ScnvL+PzY0BOU5usJgtXDVKj3zjl2R/38iNv2sHZCrhqcn1RpSU4y9g/WQ9uPxIw9jev315o7k5i5AAAAAAAAAABGMrgHOdI8drS2UXlH0u2XLw/PuJq49Sv1VuXwI00VfJb2SHHudlovB7Kmo8d8u/95HPJ5Zb018EcHL6W9dU9pk8ditu/UZiGSI67QHqIf5fFIgn7xa6b9JevzNiaE5YxdJSjm7NNOL5Svk152txvbiayjlG8JOL5fi/P7PPfpFoNYiE39uml4xk7/Ejl1C9pM9V2BNOiUfCXzS/o5UgLxtJZYYaa3MRkpLMXkA2K3Bg7P6N8BedSu1lFake92cvdq+Z06eO8jzvb+oxGNK6839Pr+x3laooxcnuSuzqPLtpLLOJxmKdWbm+O5clwR0RWEU9k3OTbL2isHtqiX2VnLu5eIk8I3pr45Y6HTaWX1E/ZIY7ljd+mzjToKgvYL1kPbj8SMS2N6/fXmjuTmLkAAAAAAAAAtylcAxNJ4zY02/tPKPfz8DaMcshus7uOTj275nQVJk6OxEac1OUda25XtnzMSWVgkqmoS4msm4//SL+m/xL5EfdM6/8xeBpMXW15yna2s72JEsLBxzlxSciyZNDc6O0xs6caeo21filvbfLtIpQy8nZVqFCKjg3+GnrxUrWur2vcjawd0ZcSTOZ+kbEtYeMPv1En3RTl+er5HPqHiOC97BrUtQ5Povny+WThsXpKrWjTp1Ja2z1tWT9K0rXTfH0VvzIqoSvnGtbsutVPT9mU26vGEllpdWtseDbePA3XRzovLEpzctSne17XlJrfb5nonOnRLgrWX1+/wDR8oslre3Ju/UzxDPJdF5Lb13Zk6d6ISowdSE9pGKvJNWklxatvsIamvU/67Y7/noavRansx/5Oksaa38vitmvgzkZxsyg1um/x7XDpuvI+qdgdrLtPRxvxiW0l4SX0fJ+pFs5S6PXujWj/wCHw1ODylbWn7Us35Xt4FhXHhikeB1+o7/USmttl5LYwukONu9mtyzfa+C7f9onhHqU2ps/5NE2TI4WbXRelY0Y21G23du6z5cP3mRyg2zopvjXHGC9jNOqpCUNRrWVr3/0YVeGb2apSi44NISnGToT1ZRlyafk7h7GYvDTOjwmnlUnGGo1rO17/wCiF14RYQ1SlJRwbZu5GdYS5AEkwCoAALcpXAJRiAarpPG9JPlNfk0SV7nLrF7HqcuTFaAAAAAASjK1mjBtnqdjox3pQf8AaiCW5bU+4jnfpHw7eHhNfYqK/dJNX87eZzahezk9B2BYlfKL6r5HntHeSdlyUdTHPl/B0f8Ar6p2dkWqHTD9FJZ/bf0PVOhuJjPCwUbXheMlyd2/fe/id2tg43NvqeI7IthPSxUenJ+f33NjpbExpUak57lF+Lasl4vIgpg5zUUdeqtjVTKc9sfiPG6z3L9/vI27aknbFLoi2/8AAVTjorJvaU+Xoln+vQ2XRbAbfE04v0YvXl7MM7eL1V4lVVHikj1naeo7nTSkt3yXr9ss9Ybud54U5bT8bVpdqi/cl+hPXsVeq/UZrjc5wAAAAADL0R66n7RrPYlo/UR2MTnLcAEkgCoBRoApGIBIAhVpRkrSSa5NXQyYlFSWGWf5fS/pw/CvkZ4madzX4Ify+l/Th+FfIcTHc1+CH8vpf04fhXyHEx3NfgjktJwSqzSSSUnZLJHRHYq7UlNpGMZIzp9C4OnKjFyhFt6124pv0nxIZt5LLT1xdabRtqdNRSSVktyRGdKSSwjVdLaGvg6y5Q1vwNS/6kdqzBnf2bZwaut/HH78vqeRnAm08o91KKlFxksp7mbgdIzpPWhOUJc1x71x7i9q7Vrshw6iOfj+beh8313/AIrU03O3s2zCf/LeMfBPmmvPn57lzSGl6la20qSnbdfKKfduv4G8u09PSv8ARHn+evoQUf8Aje0dVYnrrUoronl+iworz5+TNc2UVlkrJOcnls+k6XTVaWmNNSxGKwl+fu36npXQjQToU3UqK1SpbJ74xWaT7XvfhyOqmvhWWeV7Y1y1FihD3Y/yzp4xJinLdXCU5u8oRk+bimzKbRpKuMubRD+X0v6cPwr5DiZjua/BD+X0v6cPwr5DiY7mvwRi6UwVONKbVOCai7NRSZtGTyRXVQUG0kcmTlYXcIrzgnmnKN/NB7G0PeXmdlDBU4u6pwTW5qKTObiZbqqCeUkX2jBuEgCoAAAAAAAAAAAABxelfXVPaZ0R2Ki79RmHc2Ijr+j/AKiH+XxSIJ+8Wum/SXr8zYmhOWsTS14Sg90oteasYaysG0JcElJdOZ4lZ7nvW/vK0+jZW62KA2NnofQVfFP6uPV4zllBePF9iubwrlLY4tXr6dMvbfPwW/29TvtBdEaOHanL6yot0pLqxfOMeD7XdnXClR5nl9Z2tdqE4r2Y+C6+bOiJSqAAAAAAMPTHqanss2juRX/ps4u50FQX8F6yHtx+JGJbG1fvrzR3JzF0AAAAAAAAAAAAAAAACLprkvIGMIpso/dXkhkYRJK24GSoAAPJMboirUxdalSg5NVJbt0VJ3jrN5LJo4HBubSPc1ayqvSwsseOS9ccng6zQnQenC08Q9pL7qyprv4z8bLsOiFCXORR6vtuyfs0+yvHr9vn8TrYQSSSSSWSSVku4nKNtt5ZIGAAAAAAAAUaAI7KP3V5IZMYQ2UeS8kBhEwZAAAAAAAAAAAAAAAAAAAAAAAAIxgleySu7vtfNgy23uSBgAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAGJPSVGLadWCayacldMglqqYvDmk/MmWntksqL/AGKvSFLV1tpDVbsnrK1+V+Zn/Jq4eLiWPHJjuLOLh4XnyJ4fGU6mUJxk+SkmzMLq5+7JPyZidU4e8miFTSVGLcZVIJrJpySaNZammLw5JPzMxotksqLx5GRTqKSvFpp7mndeZNGSkspkbTTwzHjpGk5aiqQct1tZXvy7yJaipy4VJZ8yR0WqPE4vHkXMRi4U7a84xvuu0r27zay6FfvtLzNYVzn7qb8hh8XTqehOMrcpJ/kK7q7Pckn5MTrnD3k15l4kNAAAAAAAAAAAAAAAAAAAAAAAAAAAAAADlsLVjGtiNahKr9Y/Rgp2ze++4pa5xjbZmty59Engt7IydVeJqPLq8F3Tco7KjLZ6kdsm4aqTtne8V2I31riq63w4XEuWPPoR6Tidk1xZfDvn6kZTp1q9L+HhZwleclHUSjlk+d/1NXKu6+HcLDTy3jHI2Ssqpn3z5NclnPMhSxNGFfEbaKd5K3U17WvfhlwNY2013296s810z4m0q7Z01928cvHHgWlKVPC15xThCpNbNcVGTs32XWRpmVelsnFYTfs+TZtiM9TXCXNpc/NG1xuiqSw0koRvGDalZa10r3v4HddpKo6ZpRXJZz8Ujkq1NrvTbfN4/dmtxmIThg51M1nrZXvbVW7juOS2xONE7OfidVVbUroQ9OniX6EoVcTTlh4aqgntHq6is1krc9/7RvBwt1MZULCWeJ4x5Ijmp16eUbnnO3PPmdIXBVgAAAAAAAAAAjOXmAVQBUAAEWwCmqASiwCoAAAAAABRsA0dHC16dSrKCg1UnrZyd+Ntyy3lfCq+uc5Rx7Tzzyd07abIRUs8ljkXMXg61WNPWUFKFVSdm7aq5XW/sF1N1qhnGVJP0X1MVW1VOWM4ccepexuBltqdana66s03bWh5b18uRJdRPvo2177P4o0quj3Uqp+a+DGBwEo1K8p2carVlfh1r38zFOnlGyyUsYlj67mbr1KutR3j9izhdEy2VTDzacG/q5J3kle6uuxpPxZHXpJd1Kmb9np4/iZvZqo95G6G/UtzwuLlDYt01G2q6ivrOPs87Grq1cod03HG2euPI2VmmjPvUnnfHTPmX8Tox62H1LatF53edrJeLyJbNK+Krg2iRQ1C4bOLeRKrgpLEKtTtaS1aibtdcGub+XabSolG9Ww68pf35msboul1z6c1/RsdXzOw5iUWAVAAAAAAAIylYAglcAugAAAEQCgBJIAqAAAAAA2AW3K4BKMQCQAAAAAAAAIoApYAkkAVAAAAABGUrAEFmAXEgCoAAABRoAJAFQAAAAAACjQBSMbAEgAAAAAAAAACjQASAKgAAAAAAFJRuAEgCoAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAP/Z"}} className="img-fluid rounded-circle" id="company-icon"/>
							</div>
						</div>
						<div className="row weight-one align-items-center">
							<h4 className="col-11 text-center text-white lead">{this.state.infos.symbol}: {this.state.infos.companyName}</h4>
						</div>
						<div className="row  weight-one align-items-center">
							<h5 className="col-11   offset-1 text-white text-capitalize"><span className='text-dark'><FaExchangeAlt/> exhange :</span> <span className="lead">{this.state.infos.exchange}</span></h5>
						</div>
						<div className="row  weight-one align-items-center">
							<h5 className="col-11 offset-1 text-white text-capitalize"><span className='text-dark'><FaIndustry/> industry :</span>  <span className="lead">{this.state.infos.industry}</span></h5>
						</div>
						<div className="row  weight-one align-items-center">
							<h5 className="col-11 offset-1 text-white text-capitalize"><span className='text-dark'><IoIosGlobe/> website :</span> <a href={this.state.infos.website} className='text-light lead' target="_blank">consulter le siteweb</a></h5>
						</div>
						<div className="row  weight-two ">
							<p className="col-11 offset-1 text-white text-capitalize lead">
								{this.state.infos.description}
							</p>
						</div>
						<div className="row weight-one align-items-center">
							<h5 className="col-11 offset-1 text-white"><span className='text-dark'><FaUserTie/> CEO :</span> <span className="lead">{this.state.infos.CEO}</span></h5>
						</div>

						<div className="row weight-one align-items-center">
							<h5 className="col-11 offset-1 text-white"><span className='text-dark'><FaStore/> marché :</span> <span className="lead">{this.state.infos.sector}</span></h5>
						</div>

						<div className="row weight-one align-items-center" >
							<h5 className="col-11 offset-1 text-white"><span className='text-dark'><FaRegNewspaper/> news :</span> <button className=" btn btn-link text-light" onClick={()=>this.openModal()}>consulter les news</button></h5>
						</div>


					</div>
				</div>
			)
	}
}