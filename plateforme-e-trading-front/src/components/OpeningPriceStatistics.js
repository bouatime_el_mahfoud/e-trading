import React from 'react';
import {Line} from 'react-chartjs-2';
import { FaMagic } from "react-icons/fa";
import Modal from 'react-awesome-modal';
import {PredictionCalcul} from './PredictionCalcul'
export class OpeningPriceStatistics extends React.Component{
	
	constructor(props){
		super(props)
		this.state={
			data:[],
			labels:[],
			changedata:[],
			label:"",
			visible:false
			
		}
		this.filterStocks=this.filterStocks.bind(this)
		this.switch=this.switch.bind(this)
		
	}
	openModal() {
        this.setState({
            visible : true
        });
    }

    closeModal() {
        this.setState({
            visible : false
        });
    }
	componentWillMount() {
		var link='https://api.iextrading.com/1.0/stock/'+this.props.symbol+'/chart/1m?filter=open,date,minute,changePercent'
		fetch(link)
      	.then(response => response.json())
      	.then(data =>{this.filterStocks(data,1)})
      	
      	

	}
	switch(type:number){
		
		var link=''
		{(type===1)?link='https://api.iextrading.com/1.0/stock/'+this.props.symbol+'/chart/1m?filter=open,date,minute,changePercent':(type===2)?link='https://api.iextrading.com/1.0/stock/'+this.props.symbol+'/chart/6m?filter=open,date,changePercent':(type===3)?link='https://api.iextrading.com/1.0/stock/'+this.props.symbol+'/chart/1y?filter=open,date,changePercent':link=''}
		fetch(link)
      	.then(response => response.json())
      	.then(data =>{this.filterStocks(data,type)})
	}
	
	
	filterStocks(data,type){
		
		if(type===1){
			var values=[]
			var time=[]
			var changePercent=[]
			var date="ce dernier mois"
			
			data.map(stock=>values.push( parseFloat(stock.open)))
			data.map(stock=>time.push(stock.date))
			data.map(stock=>changePercent.push(parseFloat(stock.changePercent)/100))
			this.setState({data:values,labels:time,label:date,changedata:changePercent})


		}
		else if(type===2){
			var values=[]
			var time=[]
			var changePercent=[]
			var date="pour les derniers 6 mois"
			data.map(stock=>values.push( parseFloat(stock.open)))
			data.map(stock=>time.push(stock.date))
			data.map(stock=>changePercent.push(parseFloat(stock.changePercent)/100))
			this.setState({data:values,labels:time,label:date,changedata:changePercent})
			}		
		else if(type===3){
			var values=[]
			var time=[]
			var changePercent=[]
			var date="pour l'année "+data[0].date.substring(0,4)
			
			data.map(stock=>values.push( parseFloat(stock.open)))
			data.map(stock=>time.push(stock.date))
			data.map(stock=>changePercent.push(parseFloat(stock.changePercent)/100))
			this.setState({data:values,labels:time,label:date,changedata:changePercent})
		}
		
	}
	
	render(){
		var data= {
        labels: this.state.labels,
        datasets: [{
        label: "prix de l'overture de l'action pour  "+this.state.label,
        borderColor: 'rgb(38,166,154)',
        fill:false,
        data: this.state.data,
        }]
    }
		return(
				<div className='col-12 border-rounded   stats align-items-center'>
					<Modal visible={this.state.visible} effect="fadeInUp" width="80%"  onClickAway={() => this.closeModal()}>
                   				<PredictionCalcul visible={this.state.visible} stocks={this.state.changedata} stocks_date={this.state.labels} initial_stock={this.state.data} />
                	</Modal>
					<div className="row">
						<h4 className="col-11 text-right btn text-warning" onClick={() => this.openModal()}><FaMagic /> </h4>
					</div>
					<div className="row " >
							<div className="col-12">
									<Line data={data}/>
							</div>
							
					</div>
					<div className="row">
							<div className="col-12 text-center">
								<div className="btn-group mr-2" role="group" aria-label="First group">
	    							<button type="button" className="btn btn-info" onClick={()=>this.switch(1)}>1m</button>
	    							<button type="button" className="btn btn-info" onClick={()=>this.switch(2)}>6m</button>
	   								<button type="button" className="btn btn-info" onClick={()=>this.switch(3)}>1a</button>
    
  								</div>
							</div>
							
					</div>
						
					
					
				</div>
				
			)
	}
}